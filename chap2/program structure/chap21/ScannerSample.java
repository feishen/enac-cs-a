package chap21;

import java.util.Scanner;

public class ScannerSample {

	public static void main(String[] args) {
		//print the usage information on the screen
		System.out.println("Please input a line and press ENTER:");
		
		//initialize a scanner for the keyboard input
		Scanner scan = new Scanner(System.in);
		
		//read a line from the keyboard input
		String line = scan.nextLine();
		
		//print the entered line on the screen
		System.out.println(line);
	}

}
