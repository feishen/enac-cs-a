package chap21;	//package declaration

import java.lang.Math;	//import used packages or classes
//import java.lang.*;	//this also works

public class Chap21 {	//class name, which is same as the file name
	
	/*
	 * some other methods and fields can be defined here
	 */

	public static void main(String[] args) {	//the entrance of the program
		//absInt is a variable with the type 'integer'
		int absInt = Math.abs(-5);
		System.out.println("The absolute value of -5 is " + absInt);
		//Integer.MAX_VALUE is a constant
		System.out.println("The maximum integer in Java is " + Integer.MAX_VALUE);
	}

}
