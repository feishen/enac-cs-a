package mypackage;	//package name

/*
This is your first program.
Multi-line comment section
*/
public class MyProgram {	//class declaration

	public static void main(String[] args) {	//program entrance
		System.out.println("Hello world");		//statement
	}
	
}
